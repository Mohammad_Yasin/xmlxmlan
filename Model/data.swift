//
//  data.swift
//  xmlXmlAn
//
//  Created by Mohammad Yasin on 29/08/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import Foundation
import SWXMLHash
import SwiftyXMLParser
import XMLCoder


struct Book : XMLIndexerDeserializable {
    let ContentOrder: String
    let ContentHeader: String
    let ContentDetails: String
    let ContentHeader_ENG: String
    let ContentDetails_ENG: String
   
    
    static func deserialize(_ node: XMLIndexer) throws -> Book {
        return try Book(
            ContentOrder: node["ContentOrder"].value(),
            ContentHeader: node["ContentHeader"].value(),
            ContentDetails: node["ContentDetails"].value(),
            ContentHeader_ENG: node["ContentHeader_ENG"].value(),
            ContentDetails_ENG: node["ContentDetails_ENG"].value()


        )
    }
    
}
