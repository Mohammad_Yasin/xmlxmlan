//
//  ViewController.swift
//  xmlXmlAn
//
//  Created by Mohammad Yasin on 26/08/19.
//  Copyright © 2019 Mohammad Yasin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyXMLParser
import XMLCoder
import SWXMLHash
import CommonCrypto
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class ViewController: UIViewController, XMLParserDelegate, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    

    var myFeed : [Book] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        testLabel.text = Bundle.main.localizedString(forKey: "yasin", value: "", table: "en")
        parseXML()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
        self.tableView.dataSource = self
        self.tableView.delegate = self
}

    func parseXML (){
       
        func MD5(string: String) -> Data {
            let length = Int(CC_MD5_DIGEST_LENGTH)
            let messageData = string.data(using:.utf8)!
            var digestData = Data(count: length)
            
            _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
                messageData.withUnsafeBytes { messageBytes -> UInt8 in
                    if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                        let messageLength = CC_LONG(messageData.count)
                        CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                    }
                    return 0
                }
            }
            return digestData
        }
        
        let date = DateFormatter.init()
        date.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = date.string(from: Date())
        print("dateString \(dateString)")
        let md5Data = MD5(string:"\(dateString)#FM#CODELABS#201908@9C572CODELABS")
        let md5Hex = md5Data.map { String(format: "%02hhx", $0) }.joined()
        print("md5Base64: \(md5Hex)")
        let url = "https://soap.firstmedia.com/fmcomapi/Content.asmx/GETContactUs"
        //"https://soap.firstmedia.com/fmcomapi/Content.asmx/GETHelp"
        let parameters: Parameters = ["Client":"CODELABS","OtherInfo":"CODELABS","RequestDate":dateString,"Token":md5Hex]
        
        
        Alamofire.request(url, method: .get, parameters: parameters,encoding: URLEncoding.default).responseData { response in
            
            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.data as Any)   // result of response serialization
            
            if let data = response.data {
               let xml = SWXMLHash.parse(data)
                print(xml) // outputs the top title of iTunes app raning.
                
                for elem in xml["ListContactUs"]["ContactUsLists"]["ContactUs"].all {
                    
                    
                    print("hasil===== \(elem["ContentOrder"].element?.text ?? "")")
                    print("hasil===== \(elem["ContentHeader"].element?.text ?? "")")
                    print("hasil===== \(elem["ContentDetails"].element?.text ?? "")")
                    print("hasil===== \(elem["ContentHeader_ENG"].element?.text ?? "")")
                    
                }
                
                let myParser: [Book] = try! xml["ListContactUs"]["ContactUsLists"]["ContactUs"].value()
                
                self.myFeed = myParser
                print(self.myFeed)
                self.tableView.reloadData()
                
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myFeed.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")

        let ContentOrder = cell?.viewWithTag(1) as! UILabel
        let ContentHeader = cell?.viewWithTag(2) as! UILabel
        let ContentDetails = cell?.viewWithTag(3) as! UILabel
        let ContentHeader_ENG = cell?.viewWithTag(4) as! UILabel
        let ContentDetails_ENG = cell?.viewWithTag(5) as! UILabel
       
        
        let model = myFeed[indexPath.row]
        
        ContentOrder.text = model.ContentOrder as? String
        ContentHeader.text = model.ContentHeader as? String
        ContentDetails.text = model.ContentDetails as? String
        ContentHeader_ENG.text = model.ContentHeader_ENG as? String
        ContentDetails_ENG.text = model.ContentDetails_ENG as? String


        return cell!
        
        
    }

}

